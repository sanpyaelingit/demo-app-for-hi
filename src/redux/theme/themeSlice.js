import { createSlice } from '@reduxjs/toolkit'

const initialState = {
    stretch: true,
    mode: 'light',
    color: 'indigo',
}

export const themeSlice = createSlice({
    name: 'theme',
    initialState,
    reducers: {
        reset: () => {
            return initialState
        },
        toggleMode: (state) => {
            state.mode = state.mode === 'dark' ? 'light': 'dark'
        },
        toggleStretch: (state) => {           
            state.stretch = !state.stretch
        },
        changeColor: (state, action) => {           
            state.color = action.payload
        },
        changeLayout: (state, action) => {
            return {
                ...state,
                ...action.payload
            }
        }
    },
})

// Action creators are generated for each case reducer function
export const { changeColor, changeLayout, reset, toggleMode, toggleStretch } = themeSlice.actions

export default themeSlice.reducer
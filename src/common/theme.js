import JosefinSans from 'assets/fonts/JosefinSans.ttf'
import * as muicolors from '@mui/material/colors'
import { createTheme } from '@mui/material/styles'

export const colors = ['red', 'pink', 'purple', 'deepPurple', 'indigo', 'blue', 'lightBlue', 'cyan', 'teal', 'green', 'lightGreen', 'lime', 'amber', 'orange', 'deepOrange', 'brown']

function getColor(color) {
    return muicolors[color] || muicolors.indigo
}

export function getTheme(theme) {
    return createTheme({
        palette: {
            mode: theme.mode,
            primary: getColor(theme.color),
            background: {
                paper: theme.mode === 'dark' ? '#161C24' : '#fff',
                default: theme.mode === 'dark' ? '#212B36' : '#fff'
            }
        },
        typography: {
            h4: {
                lineHeight: 1.5,
                fontWeight: 700,
                fontSize: '1.5rem',
                margin: '0px 0px 8px',
            }
        },
        components: {
            MuiCssBaseline: {
                styleOverrides: `
                @font-face {
                  font-family: 'JosefinSans';
                  font-style: normal;
                  font-display: swap;
                  font-weight: 400;
                  src: local('JosefinSans'), local('JosefinSans-Regular'), url(${JosefinSans}) format('woff2');
                  unicodeRange: U+0000-00FF, U+0131, U+0152-0153, U+02BB-02BC, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2122, U+2191, U+2193, U+2212, U+2215, U+FEFF;
                }
              `,
            },
        },
    })
}

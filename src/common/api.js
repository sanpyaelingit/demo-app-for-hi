import axios from 'axios'
import { url } from './endpoint'
import { loadState } from './localStorage'

const successHandler = (response) => {
    return response
}

const errorHandler = (error) => {
    if (!error.response) {
        // console.warn('error', error)
    }

    return error.response
}

export default function api() {
    let token = getToken()
    const headers = !token ? null: {
        Authorization: `Bearer ${token}`,
    }

    const axiosInstance = axios.create({
        baseURL: url,
        headers: headers
    })

    axiosInstance.interceptors.response.use(
        (response) => successHandler(response),
        (error) => errorHandler(error)
    )

    return axiosInstance
}

function getToken() {
    const state = loadState()

    return state?.auth.token
}
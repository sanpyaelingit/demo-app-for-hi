import { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { useNavigate } from 'react-router-dom'

export function useAuth() {
    const navigate = useNavigate()
    const auth = useSelector((state) => state.auth)

    useEffect(() => {
        if (!auth.token)
            navigate('/auth/login')
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [auth])
}

export function useIsAuth() {
    const navigate = useNavigate()
    const auth = useSelector((state) => state.auth)

    useEffect(() => {
        if (auth.token)
            navigate('/')
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [auth])
}
import MuiBox from '@mui/material/Box'
import MuiMenu from '@mui/material/Menu'
import { styled } from '@mui/material/styles'

export const AvatarWrapper = styled(MuiBox)(({ theme }) => ({
    minWidth: 200,
    display: 'flex',
    padding: theme.spacing(1.5),
    paddingTop: theme.spacing(1),
    '& .MuiAvatar-root': {
        width: '48px !important',
        height: '48px !important'
    },
    '& .MuiBox-root': {
        marginLeft: theme.spacing(2)
    },
}))

export const Menu = styled(MuiMenu)(({ theme }) => ({
    '& .MuiPaper-root': {
        minWidth: 100,
        overflow: 'visible',
        filter: 'drop-shadow(0px 2px 8px rgba(0,0,0,0.32))',
        [theme.breakpoints.up('sm')]: {
            margin: theme.spacing(1, 3, 2)
        },
        '& .MuiAvatar-root': {
            width: 32,
            height: 32,
            ml: -0.5,
            mr: 1,
        },
    }
}))

export const Note = styled('div')(({ theme }) => ({
    ...theme.typography.body2,
    margin: 0,
    color: theme.palette.text.secondary
}))

// export default styles
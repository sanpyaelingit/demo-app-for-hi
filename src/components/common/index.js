import { styled } from '@mui/material/styles'
import MuiPaper from '@mui/material/Paper'

function getColor(mode) {
    if(mode === 'dark') return '0 0 0'

    return '145 158 171'
}

export const Card = styled(MuiPaper)(({ theme }) => ({
    position: 'relative',
    borderRadius: theme.spacing(1.5),
    border: '1px solid rgba(0, 0, 0, 0.12)',
    boxShadow: `rgb(${getColor(theme.palette.mode)} / 20%) 0px 0px 2px 0px, rgb(${getColor(theme.palette.mode)} / 12%) 0px 12px 24px -4px`
}))
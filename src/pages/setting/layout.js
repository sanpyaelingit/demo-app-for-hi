import { useState } from 'react'
import { Link, Outlet, useMatch } from 'react-router-dom'
// self
import menus from './menu'
import Page from 'components/page'
import { Card } from 'components/common'
// mui
import { styled } from '@mui/material/styles'
import { Grid, List, ListItem, ListItemIcon, ListItemText } from '@mui/material'

const breadcrumbs = [{ label: 'Setting' }]

const NavItem = styled(ListItem)(({ active, theme }) => ({
    borderRadius: 8,
    marginBottom: theme.spacing(.5),
    ...(active && {
        background: theme.palette.action.hover,
    }),
}))

export function Layout() {
    const [bc, setBC] = useState(breadcrumbs)

    const handleClick = (v) => () => {
        setBC([...breadcrumbs, { label: v.label }])
    }

    return (
        <Page title='Settings' breadcrumbs={bc}>
            <Grid container spacing={2}>
                <Grid item xs={12} md={3}>
                    <Card elevation={0} sx={{ p: 2 }}>
                        <List dense>
                            {menus.map((v, i) => <MenuItem key={i} item={v} onClick={handleClick(v)} />)}
                        </List>
                    </Card>
                </Grid>
                <Grid item xs={12} md={9}>
                    <Card elevation={0} sx={{ p: 2 }}>
                        <Outlet />
                    </Card>
                </Grid>
            </Grid>
        </Page>
    )
}

function MenuItem({ item, ...props }) {
    let match = useMatch(item.to)
    return (
        <NavItem button to={item.to} active={match} component={Link} {...props}>
            <ListItemIcon>
                {item.icon}
            </ListItemIcon>
            <ListItemText primary={item.label} />
        </NavItem>
    )
}
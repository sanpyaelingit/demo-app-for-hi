import { colors } from 'common/theme'
// mui
import { styled } from '@mui/material/styles'
import * as muiColors from '@mui/material/colors'
import { Box, ButtonBase, Grid, Stack } from '@mui/material'
import Brightness1Icon from '@mui/icons-material/Brightness1'
import CheckCircleIcon from '@mui/icons-material/CheckCircle'
import DarkModeIcon from '@mui/icons-material/DarkModeTwoTone'
import FirstPageIcon from '@mui/icons-material/FirstPage'
import FullscreenIcon from '@mui/icons-material/Fullscreen'
import FullscreenExitIcon from '@mui/icons-material/FullscreenExit'
import LastPageIcon from '@mui/icons-material/LastPage'
import LightModeIcon from '@mui/icons-material/LightModeTwoTone'
import WebIcon from '@mui/icons-material/Web'

const StretchWrapper = styled(ButtonBase)(({ theme }) => ({
    borderRadius: 6,
    minWidth: 200,
    width: 'fit-content',
    display: 'flex',
    justifyContent: 'center',
    padding: theme.spacing(1, 5),
    background: theme.palette.action.hover,
    borderColor: theme.palette.primary.light
}))

const StretchButton = styled(Stack)(({ theme, mode }) => ({
    borderRadius: 6,
    padding: theme.spacing(.5, 2),
    background: mode === 'dark' ? theme.palette.common.black : theme.palette.common.white,
}))

const Content = styled(Box)(({ theme, value }) => ({
    display: 'flex',
    justifyContent: 'center',
    padding: theme.spacing(0, value ? 6 : 2),
}))

const ModeItem = styled(ButtonBase)(({ theme }) => ({
    borderRadius: 6,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(2, 4),
    background: theme.palette.action.hover,
}))

const ColorItem = styled(ButtonBase)(({ theme, hex }) => ({
    color: hex,
    borderRadius: 6,
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(1, 2),
    background: theme.palette.action.hover,
    [theme.breakpoints.up('sm')]: {
        padding: theme.spacing(2, 3),
    },
}))

export const Action = styled(Box)(() => ({
    position: 'absolute',
    top: 20,
    right: 20,
}))

export const ModeSwitch = ({ value, onClick }) => {
    return (
        <Stack direction='row' spacing={2}>
            <ModeItem onClick={() => onClick()}>
                {value === 'dark' ?
                    <DarkModeIcon color='inherit' sx={{ fontSize: 30 }} /> :
                    <LightModeIcon color='inherit' sx={{ fontSize: 30 }} />
                }
            </ModeItem>
        </Stack>
    )
}

export const FsSwitch = ({ value, onClick }) => {
    return (
        <Stack direction='row' spacing={2}>
            <ModeItem onClick={() => onClick()}>
                {!value ?
                    <FullscreenIcon color='inherit' sx={{ fontSize: 30 }} /> :
                    <FullscreenExitIcon color='inherit' sx={{ fontSize: 30 }} />
                }
            </ModeItem>
        </Stack>
    )
}

export const StretchSwitch = ({ mode, value, onClick }) => {
    return (
        <StretchWrapper direction='row' onClick={() => onClick()}>
            <StretchButton direction='row' mode={mode} spacing={2} >
                {value ?
                    <FirstPageIcon /> :
                    <LastPageIcon />
                }
                <Content value={value}>
                    <WebIcon />
                </Content>
                {!value ?
                    <FirstPageIcon /> :
                    <LastPageIcon />
                }
            </StretchButton>
        </StretchWrapper>
    )
}

export const ColorPalette = (props) => {
    return (
        <Grid container spacing={2}>
            {colors.map(k => (
                <Grid key={k} item xs={3} md={1.5}>
                    <ColorItem key={k} hex={muiColors[k][500]} onClick={() => props.onChange(k)}>
                        {k === props.value ?
                            <CheckCircleIcon fontSize='large' /> :
                            <Brightness1Icon fontSize='large' />
                        }
                    </ColorItem>
                </Grid>
            ))}
        </Grid>
    )
}
import { useFullScreen } from 'common/fullscreen'
import { Action, ColorPalette, FsSwitch, ModeSwitch, StretchSwitch } from './comp'
// mui
import RestartAltIcon from '@mui/icons-material/RestartAlt'
import { Box, IconButton, Stack, Tooltip, Typography } from '@mui/material'
// redux
import { useDispatch, useSelector } from 'react-redux'
import { changeColor, reset, toggleMode, toggleStretch } from 'redux/theme/themeSlice'


function Content({ title, children }) {
    return (
        <Stack sx={{ mb: 2.5 }}>
            <Typography variant='subtitle2'>{title}</Typography>
            <Box sx={{ mt: 1 }}>
                {children}
            </Box>
        </Stack>
    )
}

export function ThemePage() {
    const el = document.getElementById('main-layout')
    const fs = useFullScreen(el)
    const dispatch = useDispatch()
    const theme = useSelector((state) => state.theme)

    const resetTheme = () => dispatch(reset())

    const toggleFullScreen = () => {
        fs.active ? fs.exit(): fs.enter()
    }

    return (
        <Box>
            <Content title='Mode'>
                <ModeSwitch value={theme.mode} onClick={() => dispatch(toggleMode())} />
            </Content>
            <Content title='Full Screen'>
                <FsSwitch value={fs.active} onClick={toggleFullScreen} />
            </Content>
            <Content title='Stretch'>
                <StretchSwitch value={theme.stretch} mode={theme.mode} onClick={() => dispatch(toggleStretch())} />
            </Content>
            <Content title='Color'>
                <ColorPalette value={theme.color} onChange={(c) => dispatch(changeColor(c))} />
            </Content>
            <Action>
                <Tooltip arrow title='Reset Setting' placement='left'>
                    <IconButton onClick={resetTheme}>
                        <RestartAltIcon />
                    </IconButton>
                </Tooltip>
            </Action>
        </Box>
    )
}
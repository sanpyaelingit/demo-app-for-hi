import { useState, useEffect } from 'react'
// api
import api from 'common/api'

const defautLimit = 6

export function useFetch() {
    const [data, setData] = useState([])
    const [loading, setLoading] = useState(true)
    const [pagination, setPagination] = useState({
        page: 1,
        size: defautLimit,
    })

    useEffect(() => {
        function fetchData() {
            setLoading(true)
            api().get(`products?page=${pagination.page}&size=${pagination.size}`)
                .then(req => req.data)
                .then((res) => {
                    const { page, size, ...value } = res
                    setData(value)
                    setLoading(false)
                })
                .catch((err) => {
                    console.warn('useFetch err:', err)
                    setLoading(false)
                })
        }
        
        fetchData()
    }, [pagination])

    const loadmore = () => {
        setPagination({
            size: defautLimit,
            page: pagination.page + 1
        })
    }

    return [data, loading, loadmore]
}

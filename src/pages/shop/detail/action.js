import { useState, useEffect } from 'react'
import { useLocation } from 'react-router-dom'

export function useDataByID() {
    let location = useLocation()
    const [data, setData] = useState({})
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        setLoading(true)
        if (location.state) {
            setData(location.state?.item)
        }

        setLoading(false)
    }, [location])

    return [data, loading]
}

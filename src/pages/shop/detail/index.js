import Page from 'components/page'
import { useDataByID } from './action'
// mui
import { styled } from '@mui/material/styles'
import { Grid, Paper, Stack, Typography } from '@mui/material'

const Image = styled('img')(({ theme }) => ({
    width: '100%',
    objectFit: 'contain'
}))

export function Detail() {
    let [data, loading] = useDataByID()

    if (loading) return 'loading' // minor

    return (
        <Page title='Product Detail' breadcrumbs={[{ label: 'Shop', to: '/shop' }, { label: data.name }]}>
            <Grid container spacing={4}>
                <Grid item sm={12} md={4}>
                    <Paper sm={{position: 'relative'}}>
                        <Image src={data.image} alt={data.name} />
                    </Paper>
                </Grid>
                <Grid item sm={12} md={8}>
                    <Stack>
                        <Typography>{data.name}</Typography>
                        <Typography>{data.amount}</Typography>
                        <Typography>{data.description}</Typography>
                    </Stack>
                </Grid>
            </Grid>
        </Page>
    )
}
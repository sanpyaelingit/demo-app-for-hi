import { Title } from 'components/page'
import { useSnackbar } from 'notistack'
import { Link, useNavigate } from 'react-router-dom'
// action
import { register } from '../action'
// form
import * as yup from 'yup'
import { useFormik } from 'formik'
import { Input } from 'components/form'
// mui
import { styled } from '@mui/material/styles'
import { Box, Button, Typography } from '@mui/material'

const Content = styled(Box)(({ theme }) => ({
    height: '80vh',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    padding: theme.spacing(0, 5)
}))

const Form = styled('form')(({ theme }) => ({
    [theme.breakpoints.up('md')]: {
        width: 500
    },
}))

const Label = styled(Typography)(({ theme }) => ({
    ...theme.typography.h4,
    fontFamily: 'JosefinSans, sans-serif',
}))

const Action = styled(Box)(({ theme }) => ({
    textAlign: 'center',
}))

const LinkText = styled(Link)(({ theme }) => ({
    fontWeight: 500,
    textDecoration: 'none',
    color: theme.palette.text.secondary
}))

const validationSchema = yup.object({
    username: yup
        .string('Enter your username')
        .required('Username is required'),
    password: yup
        .string('Enter your password')
        .min(8, 'Password should be of minimum 8 characters length')
        .required('Password is required'),
})

export function RegisterPage() {
    const navigate = useNavigate()
    const { enqueueSnackbar } = useSnackbar()
    const formik = useFormik({
        initialValues: {
            username: '',
            password: '',
        },
        validationSchema: validationSchema,
        onSubmit: async (values) => {
            const data = await register(values)
            if(!data.token) {
                enqueueSnackbar('Account creation failed', { variant: 'error' })
            } else {
                enqueueSnackbar('Account creation success', { variant: 'success' })
                navigate('/auth/login')
            }
        },
    })

    return (
        <Box>
            <Title title='Sign Up' />
            <Content>
                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                    }}
                >
                    <Label>Get your free account now.</Label>
                    <Form onSubmit={formik.handleSubmit}>
                        <Input
                            formik={formik}
                            name='username'
                            label='Username'
                            autoComplete='username'
                            autoFocus
                        />
                        <Input
                            formik={formik}
                            name='password'
                            label='Password'
                            type='password'
                            autoComplete='current-password'
                        />
                        <Button
                            type='submit'
                            fullWidth
                            variant='contained'
                            sx={{ mt: 3, mb: 2 }}
                        >
                            Sign Up
                        </Button>
                        <Action>
                            <LinkText to='/auth/login' variant='body2'>
                                Already have account?
                            </LinkText>
                        </Action>
                    </Form>
                </Box>
            </Content>
        </Box>
    )
}
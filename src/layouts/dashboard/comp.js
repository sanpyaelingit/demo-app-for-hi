import MuiBox from '@mui/material/Box'
import MuiDrawer from '@mui/material/Drawer'
import MuiList from '@mui/material/List'
import MuiListItem from '@mui/material/ListItem'
import { alpha, styled } from '@mui/material/styles'

export const mixin = (theme) => ({
    borderRight: 0,
    overflowX: 'hidden',
    width: `calc(${theme.spacing(9)} + 1px)`,
    backgroundColor: theme.palette.primary.main,
})

export const Drawer = styled(MuiDrawer)(
    ({ theme }) => ({
        flexShrink: 0,
        whiteSpace: 'nowrap',
        boxSizing: 'border-box',
        ...mixin(theme),
        '& .MuiDrawer-paper': mixin(theme),
    }),
)

export const Logo = styled(MuiBox)(
    ({ theme }) => ({
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        padding: theme.spacing(.5, 0),
        '& img': {
            width: 60,
            height: 60
        }
    }),
)

export const List = styled(MuiList)(({ theme }) => ({
    margin: 0,
    padding: theme.spacing(2, 1),
}))

export const ListItem = styled(MuiListItem)(({ active, theme }) => ({
    borderRadius: 5,
    display: 'flex',
    alignItems: 'center',
    position: 'relative',
    justifyContent: 'flex-start',
    marginBottom: theme.spacing(0.5),
    ':hover': {
        background: alpha(theme.palette.common.black, .15),
    },
    ...(active && {
        background: alpha(theme.palette.common.black, .2),
        '& .MuiListItemIcon-root': {
            color: alpha(theme.palette.common.white, 1),
        },
        '& .MuiListItemText-root': {
            color: alpha(theme.palette.common.white, 1),
        }
    }),
    ...(!active && {
        '& .MuiListItemIcon-root': {
            color: alpha(theme.palette.common.white, .6),
        },
        '& .MuiListItemText-root': {
            color: alpha(theme.palette.common.white, .6),
        }
    }),
}))

export const AvatarWrapper = styled(MuiBox)(({ theme }) => ({
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(2),
}))

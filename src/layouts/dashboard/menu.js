import ArticleOutlinedIcon from '@mui/icons-material/ArticleOutlined'
import BugReportOutlinedIcon from '@mui/icons-material/BugReportOutlined'
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined'
import SettingsOutlinedIcon from '@mui/icons-material/SettingsOutlined'
import StorefrontOutlinedIcon from '@mui/icons-material/StorefrontOutlined'

const menus = [
    {
        to: '/',
        label: 'Dashboard',
        icon: <HomeOutlinedIcon/>
    },
    {
        to: '/shop',
        label: 'Shop',
        icon: <StorefrontOutlinedIcon/>
    },
    {
        to: '/post',
        label: 'Post',
        icon: <ArticleOutlinedIcon/>
    },
    {
        to: '/404',
        label: 'Page',
        icon: <BugReportOutlinedIcon/>
    },
    {
        to: '/setting/theme',
        label: 'Setting',
        icon: <SettingsOutlinedIcon/>
    },
]

export default menus
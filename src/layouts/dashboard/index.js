import MobileLayout from './mobile'
import DesktopLayout from './desktop'
import { useAuth } from 'common/hook'
import { Outlet } from 'react-router-dom'
// mui
import { useTheme } from '@mui/material/styles'
import useMediaQuery from '@mui/material/useMediaQuery'

export default function Dashboard() {
    useAuth()
    const theme = useTheme()
    const matches = useMediaQuery(theme.breakpoints.up('sm'))

    if (!matches) return (
        <MobileLayout>
            <Outlet />
        </MobileLayout>
    )

    return (
        <DesktopLayout>
            <Outlet />
        </DesktopLayout>
    )
}
